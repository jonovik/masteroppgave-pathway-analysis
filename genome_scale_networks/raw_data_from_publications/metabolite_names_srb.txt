0 M_glc6p_srb glucose 6-phosphate internal both
1 M_CO2_srb molecular oxygen internal both
2 M_NADH_srb reduced nicotinamide adenine dinucleotide internal both
3 M_rbl5p_srb ribulose 5-phosphate internal both
4 M_rbo5p_srb ribose 5-phosphate internal both
5 M_xll5p_srb xylulose 5-phosphate internal both
6 M_ery4p_srb erythrose 4-phosphate internal both
7 M_fru6p_srb fructose 6-phosphate internal both
8 M_ga3p_srb glyceraldehyde 3-phosphate internal both
9 M_ATP_srb adenosine 5’-triphosphate internal both
10 M_oaa_srb oxaloacetate internal both
11 M_PEP_srb phosphoenolpyruvate internal both
12 M_pyr_srb pyruvate internal both
13 M_ac_CoA_srb acetyl coenzyme A internal both
14 M_icit_srb isocitrate internal both
15 M_akg_srb α-ketoglutarate internal both
16 M_succ_srb succinate internal both
17 M_SO4ex_gen sulfate external both
18 M_H2Sex_gen sulfide external both
19 M_glyox_srb glyoxylate internal both
20 M_H2_srb molecular hydrogen internal both
21 M_ATPex_srb adenosine 5’-triphosphate external output
22 M_NH3_srb ammonia internal both
23 M_bm_srb biomass external output
24 M_CO2ex_gen molecular oxygen external both
25 M_NH3ex_gen ammonia external both
26 M_H2pool_gen molecular hydrogen external both
27 M_ac_pool acetate external both
28 objective Virtual objective metabolite external output
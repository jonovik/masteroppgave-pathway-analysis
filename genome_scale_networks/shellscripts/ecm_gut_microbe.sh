#!/bin/bash

#SBATCH -J LP                        # Name of job
#SBATCH -o LP-%j.out                 # Write the standard output to file
#SBATCH -e LP-%j.err                 # Write the standard error to file
#SBATCH --nodes=10                   # Request N nodes
#SBATCH --ntasks=32                  # Request n cores or task per node
#SBATCH --mem=10G                    # Request 10GB RAM per core

module load OpenMPI/4.0.5-gcccuda-2020b
module load Gurobi
module load lrslib/6.2

# Run program in parallel without container
mpiexec python ecmtool/main.py --model_path models/Lactobacillus_plantarum_WCFS1.xml --out_path results/LP_conversions.csv --print_conversions false --hide_all_in_or_outputs inputs

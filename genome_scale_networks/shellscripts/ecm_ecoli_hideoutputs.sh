#!/bin/bash

#SBATCH -J ecoli_hide               # Name of job
#SBATCH -o ecoli_hide-%j.out        # Write the standard output to file
#SBATCH -e ecoli_hide-%j.err        # Write the standard error to file
#SBATCH --nodes=1                   # Request N nodes
#SBATCH --ntasks=1                  # Request n cores or task per node
#SBATCH --mem=8G                    # Request 8 GB RAM per core

module purge
module load Gurobi
module load lrslib/6.2

python ecmtool/main.py --model_path ecmtool/models/e_coli_core.xml --direct false --hide_all_in_or_outputs output

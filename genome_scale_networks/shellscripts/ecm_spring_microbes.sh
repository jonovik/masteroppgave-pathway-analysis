#!/bin/bash

#SBATCH -J spring_microbes          # Name of job
#SBATCH -o spring_microbes-%j.out   # Write the standard output to file
#SBATCH -e spring_microbes-%j.err   # Write the standard error to file
#SBATCH --nodes=1                   # Request N nodes
#SBATCH --ntasks=1                  # Request n cores or task per node
#SBATCH --mem=8G                    # Request 8 GB RAM per core

module purge
module load Gurobi
module load lrslib/6.2

python ecmtool/main.py --model_path models/microbe_spring.xml --add_objective_metabolite false --use_external_compartment 'e' --hide 19,21,48,56,59,73,74,81
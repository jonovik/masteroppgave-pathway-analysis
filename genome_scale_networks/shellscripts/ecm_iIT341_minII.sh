#!/bin/bash

#SBATCH -J minII_ecm             # Name of job
#SBATCH -o minII_ecm-%j.out      # Write the standard output to file
#SBATCH -e minII_ecm-%j.err      # Write the standard error to file
#SBATCH --nodes=1                # Request N nodes
#SBATCH --ntasks=4               # Request n cores or task per node
#SBATCH --mem=8G                 # Request 8GB RAM per core

module load OpenMPI/4.0.5-gcccuda-2020b
module load Gurobi
module load lrslib/6.2

# Run program in parallel without container
mpiexec -n 4 python ecmtool/main.py --model_path ecmtool/models/iIT341.xml --direct false --inputs 139,262,28,294,300,306,314,231,35,350,259,261,22,356,334,93,293,271 --out_path results/iIT_minII.csv --outputs 16,26,29,33,39,40,59,65,75,81,90,100,110,145,171,174,212,223,224,232,234,235,239,252,253,255,263,265,269,276,277,279,280,283,284,286,291,296,302,308,312,319,320,323,325,329,331,336,341,342,344,345,352,358,361,366,368,370,372,293 --splitting_before_polco false

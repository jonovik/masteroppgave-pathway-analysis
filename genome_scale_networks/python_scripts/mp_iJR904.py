# -*- coding: utf-8 -*-
"""
Script for enumeration of minimal pathways (MPs). All MPs found are stored in a set,
where each MP is stored as a frozenset.

A dictionary with key pair ID:metabolite name is created as well
"""

import cobra
import mptool as mpt

model_name = 'iJR904'

# Load model
model = cobra.io.read_sbml_model('../models/{}_ecm.xml'.format(model_name))

# Define growth requirements for model for analysis of flux bounds
model.reactions.BIOMASS_Ecoli.lower_bound = 0.1

# Read index-metabolite file
metabfile = open('../raw_data_from_publications/metabolite_names_{}.txt'.format(model_name))
metabnames = metabfile.readlines()

index_to_id = dict()
for line in metabnames:
    index_to_id[int(line.split()[0])] = line.split()[1].partition('M_')[2]

# Input of ecmtool's 'hide' argument
IDs = [3, 6, 20, 27, 65, 68, 77, 120, 122, 124, 130, 133, 136, 144, 146, 164, 167, 169, 173, 180,
       189, 192, 195, 197, 206, 211, 219, 223, 229, 233, 236, 242, 244, 247, 249, 260, 265, 268,
       280, 286, 288, 305, 315, 324, 328, 333, 336, 338, 340, 349, 352, 354, 356, 358, 360, 363,
       366, 376, 378, 380, 382, 388, 393, 395, 397, 399, 402, 404, 413, 419, 424, 427, 431, 436,
       447, 452, 455, 459, 462, 466, 474, 476, 479, 481, 490, 493, 497, 500, 502, 504, 506, 509,
       510, 513, 515, 517, 525, 532, 534, 536, 542, 545, 547, 549, 555, 562, 583, 589, 592, 603,
       611, 616, 623, 633, 638, 643, 649, 651, 660, 662, 668, 674, 680, 682, 688, 692, 695, 697,
       699, 704, 707, 709, 711, 714, 742, 745, 747, 750, 752, 755, 759]

# Select IDs to exclude in subset
hide_set = set()
for i in IDs:
    hide_set.add(index_to_id[i])

# Create subset
subset = list(r.id for r in model.exchanges if r.id.partition('EX_')[2] not in hide_set)

# Add biomass reaction
subset.append('BIOMASS_Ecoli')

# Split reactions
irreversible = mpt.make_irreversible(model=model, subset=subset)
subset_irreversible = set(r.id for r in irreversible)

# Enumerate minimal sets of exchange reactions
mps, mcs, complete = mpt.find_mps(model,
                                  subset=subset_irreversible,
                                  method='iterative',
                                  graph=True,
                                  random=False,
                                  bounds=0,
                                  tol=1e-09,
                                  inf=1000,
                                  threads=0,
                                  max_mps=0,
                                  max_t=0,
                                  verbose=True,
                                  export=False)

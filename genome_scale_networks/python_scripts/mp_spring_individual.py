# -*- coding: utf-8 -*-
"""
Script for enumeration of minimal pathways (MPs). All MPs found are stored in a set,
where each MP is stored as a frozenset.
"""
import cobra
import mptool as mpt

model_names = ['srb', 'fap', 'syn']

# Load models
models = []
for name in model_names:
    model = cobra.io.read_sbml_model(f'genome_scale_networks/models/spring_microbes/{name}.xml')
    models.append(model)

# Define growth requirements for model for analysis of flux bounds
models[0].objective.lower_bound = 0.1
models[1].objective.lower_bound = 0.1
models[2].objective.lower_bound = 0.1

for model in models:
    # Define exchange reactions as subset for finding MPs
    subset = list(r.id for r in model.exchanges)

    # Split reactions
    irreversible = mpt.make_irreversible(model=model, subset=subset)
    subset_irreversible = set(r.id for r in irreversible)

    # Enumerate minimal sets of exchange reactions
    mps, mcs, complete = mpt.find_mps(model, 
                                      subset=subset_irreversible, 
                                      method='iterative', 
                                      graph=True, 
                                      random=False, 
                                      bounds=0, 
                                      tol=1e-09, 
                                      inf=1000, 
                                      threads=0, 
                                      max_mps=0, 
                                      max_t=0, 
                                      verbose=True,
                                      export=False)

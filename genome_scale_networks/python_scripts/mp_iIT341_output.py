# -*- coding: utf-8 -*-
"""
Script for enumeration of minimal pathways (MPs) of iIT341 output metabolites.

All MPs found are stored in a set, where each MP is stored as a frozenset.

A dictionary with key pair ID:metabolite name is created as well if METABOLITE_NAMES = True
"""

import cobra
import mptool as mpt

model_name = 'iIT341'

# Load model
model = cobra.io.read_sbml_model('../models/{}_ecm_minII.xml'.format(model_name))

# Define growth requirements for model for analysis of flux bounds
model.reactions.BIOMASS_HP_published.lower_bound = 0.1

# Define exchange reactions as subset for finding MPs
metabfile = open('../raw_data_from_publications/metabolite_names_{}.txt'.format(model_name))
metabnames = metabfile.readlines()

inputs = set()
for line in metabnames:
    if line.split()[-1] not in ('output', 'both'):
        inputs.add(line.split()[1].partition('M_')[2])

subset = list(r.id for r in model.exchanges if r.id.partition('EX_')[2] in inputs)

# Split reactions
irreversible = mpt.make_irreversible(model=model, subset=subset)
subset_irreversible = set(r.id for r in irreversible)

# Enumerate minimal sets of exchange reactions
mps, mcs, complete = mpt.find_mps(model,
                                  subset=subset_irreversible,
                                  method='iterative',
                                  graph=True,
                                  random=False,
                                  bounds=0,
                                  tol=1e-09,
                                  inf=1000,
                                  threads=0,
                                  max_mps=0,
                                  max_t=0,
                                  verbose=False,
                                  export=True)

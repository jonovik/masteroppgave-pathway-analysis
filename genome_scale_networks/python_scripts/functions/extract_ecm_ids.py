def extract_ecm_ids(ID_file):
    """
    Returns a dictionary of ecmtool's internal IDs for each metabolite and the metabolite name as key:value pair.
    
        Parameters:
            ID_file (str): Path to file containing ID and metabolite on each row. This should be automatically created by ecmtool.
        Returns:
            Dictionary of ecmtool ID and name as key:value pair.
    """
    metabfile = open(ID_file)
    metabnames = metabfile.readlines()
    
    id_to_name = dict()
    for line in metabnames:
        id_to_name[int(line.split()[0])] = line.split()[1].partition('M_')[2] # ' '.join(line.split()[2:-2]) if I should extract metabolite name
    
    return id_to_name

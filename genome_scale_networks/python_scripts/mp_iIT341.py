# -*- coding: utf-8 -*-
"""
Script for enumeration of minimal pathways (MPs). All MPs found are stored in a set,
where each MP is stored as a frozenset.

A dictionary with key pair ID:metabolite name is created as well
"""
import cobra
import mptool as mpt

model_name = 'iIT341'
METABOLITE_NAMES = False  # Save metabolite names to ID as dictionary

# Load model
model = cobra.io.read_sbml_model('../models/{}_ecm_minII.xml'.format(model_name))

# Define growth requirements for model for analysis of flux bounds
model.reactions.BIOMASS_HP_published.lower_bound = 0.1

# Define subset of reactions.
# Subset is equivalent to inputs and outputs defined in the respective ecm shellscript.
# (See processing_for_recreating_experiments.ipynb under 'genome_scale_networks')
subset = ['EX_pime_e', 'EX_phe__L_e', 'EX_pheme_e', 'EX_pi_e', 'EX_fe2_e', 'EX_fe3_e', 'EX_for_e',
          'EX_fum_e', 'EX_gal_e', 'EX_glc__D_e', 'EX_gln__L_e', 'EX_glu__L_e', 'EX_gly_e',
          'EX_gsn_e', 'EX_gua_e', 'EX_h2_e', 'EX_h2co3_e', 'EX_h2o_e', 'EX_h_e', 'EX_his__L_e',
          'EX_hxan_e', 'EX_ile__L_e', 'EX_lac__L_e', 'EX_leu__L_e', 'EX_lys__L_e', 'EX_mal__L_e',
          'EX_met__L_e', 'EX_na1_e', 'EX_nh4_e', 'EX_ni2_e', 'EX_nmn_e', 'EX_no2_e', 'EX_no3_e',
          'EX_no_e', 'EX_o2_e', 'EX_orn_e', 'EX_orot_e', 'EX_pro__L_e', 'EX_pyr_e', 'DM_hmfurn_c',
          'EX_ser__D_e', 'EX_ser__L_e', 'EX_so4_e', 'EX_succ_e', 'EX_thm_e', 'EX_thr__L_e',
          'EX_thymd_e', 'EX_trp__L_e', 'EX_tyr__L_e', 'EX_ura_e', 'EX_urea_e', 'EX_uri_e',
          'EX_val__L_e', 'EX_xan_e', 'EX_etoh_e', 'EX_aa_e', 'EX_ac_e', 'EX_acac_e', 'EX_acald_e',
          'EX_ad_e', 'EX_ade_e', 'EX_adn_e', 'EX_akg_e', 'EX_ala__D_e', 'EX_ala__L_e',
          'EX_arg__L_e', 'EX_asn__L_e', 'EX_asp__L_e', 'EX_cit_e', 'EX_co2_e', 'EX_cys__L_e',
          'EX_cytd_e', 'EX_dad_2_e', 'EX_dcyt_e', 'EX_duri_e', 'SK_ahcys_c', 'DM_amob_c']

# Split reactions
irreversible = mpt.make_irreversible(model=model, subset=subset)
subset_irreversible = set(r.id for r in irreversible)

# Enumerate minimal sets of exchange reactions
mps, mcs, complete = mpt.find_mps(model,
                                  subset=subset_irreversible,
                                  method='iterative',
                                  graph=True,
                                  random=False,
                                  bounds=0,
                                  tol=1e-09,
                                  inf=1000,
                                  threads=0,
                                  max_mps=0,
                                  max_t=0,
                                  verbose=False,
                                  export=True)

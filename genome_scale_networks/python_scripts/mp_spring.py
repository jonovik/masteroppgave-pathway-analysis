# -*- coding: utf-8 -*-
"""
Script for enumeration of minimal pathways (MPs). All MPs found are stored in a set,
where each MP is stored as a frozenset. 
"""
import gurobipy as grb
import mptool as mpt

grb_model = grb.read('genome_scale_networks/models/spring_microbes/microbe_spring.lp')

grb_model.setParam('OptimalityTol', 1e-9)
grb_model.setParam('FeasibilityTol', 1e-9)

# Select exchanges
exchanges = ['DM_bm_srb', 'DM_bm_fap', 'DM_bm_syn', 'EX_SO4ex_gen', 'EX_H2Sex_gen', 'EX_O2ex_gen', 'SK_NH3_syn', 'SK_hv_gen', 'EX_H2ex_gen', 'EX_CO2ex', 'syn22', 'fap34', 'fap21', 'syn13', 'srb23', 'fap37', 'syn31', 'srb29', 'srb24', 'fap24', 'syn14', 'syn20', 'fap35']
#exchanges = [v.VarName for v in grb_model.getVars() if v.VarName[:2] in ['DM', 'EX', 'SK']]

# Split reactions
exchanges = mpt.make_irreversible(model=grb_model, subset= [grb_model.getVarByName(v) for v in exchanges])

# Add constraint that requires net flux though the system
#grb_model.addLConstr(sum([r for r in grb_model.getVars()]) >= 1, 'network_flux')
grb_model.getVarByName('DM_bm_fap').lb = 0.1
grb_model.getVarByName('DM_bm_srb').lb = 0.1
grb_model.getVarByName('DM_bm_syn').lb = 0.1
grb_model.update()

# Define exchange reactions as subset for finding MPs
#subset = set(v.varName for v in irreversible_fluxes if v.varName in exchanges)

# Enumerate minimal sets of exchange reactions
mps, mcs, complete = mpt.find_mps(grb_model,
                                  subset=[v.VarName for v in exchanges],
                                  method='iterative',
                                  graph=True,
                                  random=False,
                                  bounds={},
                                  tol=1e-09,
                                  inf=1000,
                                  threads=0,
                                  max_mps=0,
                                  max_t=0,
                                  verbose=True,
                                  export=True)


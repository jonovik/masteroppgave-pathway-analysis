# -*- coding: utf-8 -*-
"""
Script for enumeration of minimal pathways (MPs). All MPs found are stored in a set,
where each MP is stored as a frozenset.

A dictionary with key pair ID:metabolite name is created as well
"""
import cobra
import mptool as mpt


model_name = 'salarecon_no_sd'
MINIMAL_MEDIUM = True

# Load model
model = cobra.io.read_sbml_model('../models/{}.xml'.format(model_name))

# Define growth requirements for model for analysis of flux bounds
model.reactions.Biomass.lower_bound = 0.1

# Minimal medium IDs
minimal_medium = ['EX_his__L_e', 'EX_o2_e', 'EX_ile__L_e', 'EX_leu__L_e', 'EX_lys__L_e',
                  'EX_met__L_e', 'EX_phe__L_e', 'EX_thr__L_e', 'EX_trp__L_e', 'EX_val__L_e',
                  'EX_arg__L_e', 'EX_chol_e', 'EX_pi_e', 'EX_urea_e', 'EX_co2_e']

# Define exchange reactions as subset for finding MPs
subset = list(r.id for r in model.exchanges)

# Split reactions
if MINIMAL_MEDIUM:
    irreversible = mpt.make_irreversible(model=model, subset=minimal_medium)
    subset_irreversible = set(r.id for r in irreversible)
else:
    irreversible = mpt.make_irreversible(model=model, subset=subset)
    subset_irreversible = set(r.id for r in irreversible)

# Enumerate minimal sets of exchange reactions
mps, mcs, complete = mpt.find_mps(model,
                                  subset=subset_irreversible,
                                  method='iterative',
                                  graph=True,
                                  random=False,
                                  bounds=0,
                                  tol=1e-09,
                                  inf=1000,
                                  threads=0,
                                  max_mps=0,
                                  max_t=0,
                                  verbose=True,
                                  export=False)

# Unbiased Analysis of Metabolic Networks
Different methods of unbiased analysis were applied to models ranging in size and complexity to explore the capabilities, challanges and potential of unbiased methods. Two pathway methods and one sampling method was applied to the selection of models; `ecmtool` (Clement et al. 2021), `mptool` (Øyås & Stelling, 2020) and `pta` (Gollub et al. 2021) respectively. 

# Installation
All python implementations for the methods were installed according to the installation guide provided on respective gitpages. ([ecmtool](https://github.com/SystemsBioinformatics/ecmtool), [mptool](https://gitlab.com/csb.ethz/mptool), [pta](https://gitlab.com/csb.ethz/pta)). 

`mptool` and `pta`'s sampler require gurobi as solver. Obtain a licence from [Gurobi](https://www.gurobi.com/) and install the software according to their installation guide. 

In addition, `pta` requires a C++ compiler that supports C++ 17. On a windows OS, I could follow these guides for [installation of C++ compiler](https://code.visualstudio.com/docs/cpp/config-mingw) and for [configuring the compiler on anaconda](https://python-at-risoe.pages.windenergy.dtu.dk/compiling-on-windows/configuration.html).

The anaconda environment used for this project can be recreated by entering the command: `conda create --name ENV_NAME --file unbiased_analysis.yaml`.
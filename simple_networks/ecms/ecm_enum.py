# -*- coding: utf-8 -*-
"""Constructing ECMs for E.coli core model.
    Code found on README page of the ecmtool project."""

from ecmtool.network import extract_sbml_stoichiometry
from ecmtool.conversion_cone import get_conversion_cone
from ecmtool.helpers import unsplit_metabolites, print_ecms_direct
import csv
import numpy as np
import pandas as pd
import time

DETERMINE_INPUTS_OUTPUTS = True  # Try to infer directionality (input/output/both)
EXPORT_RESULTS_IN_TXT_FILE = False  # True if results should be exported in txt format
EXPORT_RESULTS_DATAFRAME = False  # Conversion cone exported as pandas dataframe
METABOLITE_NAMES = False  # Save metabolite names to ID as dictionary
PRINT_CONVERSIONS = False  # Print all conversions calculated

model_name = 'e_coli_core'  # Name of SBML model

network = extract_sbml_stoichiometry(f'models/{model_name}.xml', add_objective=True,
                                     determine_inputs_outputs=DETERMINE_INPUTS_OUTPUTS)

# Some steps of compression only work when cone is in one orthant,
# so we need to split external metabolites with direction "both" into two metabolites,
# one of which is output, and one is input
tic = time.time()
network.split_in_out(only_rays=False)

# It is generally a good idea to compress the network before computation
network.compress(verbose=True, SCEI=True, cycle_removal=True, remove_infeasible=True)

stoichiometry = network.N

ecms = get_conversion_cone(stoichiometry, network.external_metabolite_indices(),
                           network.reversible_reaction_indices(),
                           network.input_metabolite_indices(),
                           network.output_metabolite_indices(), verbose=True)

# Since we have split the "both" metabolites, we now need to unsplit them again
cone_transpose, ids = unsplit_metabolites(np.transpose(ecms), network)
cone = np.transpose(cone_transpose)

# We can remove all internal metabolites, since their values are zero in the conversions
# (by definition of internal)
internal_ids = []
for metab in network.metabolites:
    if not metab.is_external:
        id_ind = [ind for ind, ID in enumerate(ids) if ID == metab.id]
        if len(id_ind):
            internal_ids.append(id_ind[0])

ids = list(np.delete(ids, internal_ids))
cone = np.delete(cone, internal_ids, axis=1)

toc = time.time() - tic
print(toc)
if PRINT_CONVERSIONS:
    print_ecms_direct(np.transpose(cone), ids)

# Write the calculated ecms in a txt file or dataframe.
if EXPORT_RESULTS_IN_TXT_FILE:
    ecm_results = open("results/{}_ecm_results.txt".format(model_name), "w")
    for metabolites in cone:
        np.savetxt(ecm_results, metabolites)
    ecm_results.close()
elif EXPORT_RESULTS_DATAFRAME:
    ecm_df = pd.DataFrame(cone, columns=ids)
    ecm_df.to_csv('results/{}_ecm_df.csv'.format(model_name))

if METABOLITE_NAMES:
    # Create a dataframe containing sensible names for later
    metabolite_names = pd.DataFrame({'id': network.uncompressed_metabolite_ids,
                                     'name': network.uncompressed_metabolite_names})

    # Modify ids like in .unsplit_metabolites() method so I can filter out names I don't need
    metabolite_names = metabolite_names.applymap(lambda x:
                                                 x.replace('_virtin', '').replace('_virtout', ''))

    # Select metabolites from ecm results
    metabolite_names = metabolite_names[metabolite_names.id.isin(ids)]

    # Make dictionary with ids as keys and names as values.
    ids_to_names_list = metabolite_names.set_index('id').T.to_dict('records')

    # Unlist dictionary
    for d in ids_to_names_list:
        ids_to_names = d

    # Export dictionary to csv file.
    file = open('results/{}_metabnames.csv'.format(model_name), "w")
    writer = csv.writer(file)
    for key, value in ids_to_names.items():
        writer.writerow([key, value])
    file.close()
